#ifndef ETHERNET_PACKET_H
#define ETHERNET_PACKET_H

#include "ethernet_hdr.h"
#include <string>


using namespace std ;
///
/// \brief The ethernet_packet class contains function to
/// change the ethernet information in bytes from the ethernet packet
/// header to human readable data types (strings)
///
class ethernet_packet
{

    sniff_ethernet ethernet ; /**< Ethernet header / encabezado Ethernet */

    /// \fn string ethernet_packet::mac2string(u_char srcmac[])
    /// \~English
    /// \brief Function to change the array of bytes representing a Hardware Address
    /// to its standard string representation.
    /// \param srcmac array with an Ethernet address
    /// \return Ethernet address in string format
    /// \~Spanish
    /// \brief Funcion para cambiar el arreglo de bytes que representa una direccion de hardware
    /// a su representacion estándar de caracteres.
    /// \param srcmac arreglo con una direccion Ethernet
    string mac2string(u_char []) ;

public:

    /// \fn ethernet_packet::ethernet_packet()
    /// \~English
    /// \brief Constructor, does nothing
    /// \~Spanish
    /// \brief Constructor, no hace nada
    ethernet_packet();

    /// \fn void ethernet_packet::setEtherDHost(u_char dhost[])
    /// \~English
    /// \brief Receives the array of 6 bytes of a destination host
    /// \param dhost destination host Ethernet address
    /// \~Spanish
    /// \brief Recibe el arreglo de 6 bytes de un computadora destino
    /// \param dhost direccion Ethernet de computadora destino
    void setEtherDHost(u_char []) ;

    /// \fn void ethernet_packet::setEtherSHost(u_char shost[])
    /// \~English
    /// \brief Receives the array of 6 bytes of a source host
    /// \param shost source host Ethernet address
    /// \~Spanish
    /// \brief Recibe el arreglo de 6 bytes de la computadora de origen
    /// \param shost direccion de Ethernet de computadora de origen
    void setEtherSHost(u_char []) ;

    /// \fn void ethernet_packet::setEtherType(u_short type)
    /// \~English
    /// \brief Set the Ethernet type
    /// \param type Ethernet type
    /// \~Spanish
    /// \brief Ajusta el tipo de Ethernet
    /// \param type tipo de Ethernet
    void setEtherType(u_short) ;

    /// \fn string ethernet_packet::getEtherDHost()
    /// \~English
    /// \brief Returns the destination HA in string format
    /// \return the destination HA in string format
    /// \~Spanish
    /// \brief Devuelve la direccion de hardware destino en formato de caracteres
    /// \return la direccion de hardware destino en formato de caracteres
    string getEtherDHost() ;

    /// \fn string ethernet_packet::getEtherSHost()
    /// \~English
    /// \brief Returns the source HA in string format
    /// \return the source HA in string format
    /// \~Spanish
    /// \brief Devuelve la direccion de hardware fuente en formato de caracteres
    /// \return la direccion de hardware fuente en formato de caracteres
    string getEtherSHost() ;

    /// \fn u_short ethernet_packet::getEtherType()
    /// \~English
    /// \brief Returns the Ethernet type
    /// \return the Ethernet type.
    /// \~Spanish
    /// \brief Devuelve el tipo de Ethernet
    /// \return el tipo de Ethernet
    u_short getEtherType() ;

};

#endif // ETHERNET_PACKET_H
